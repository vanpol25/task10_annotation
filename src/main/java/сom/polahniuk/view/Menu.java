package сom.polahniuk.view;

import org.apache.logging.log4j.*;
import сom.polahniuk.model.ClassInfo;
import сom.polahniuk.model.MyAnnotation;
import сom.polahniuk.model.MyClass;
import сom.polahniuk.model.Printable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Main view of project. Show menu in console.
 */
public class Menu {

    private Logger log = LogManager.getLogger(Menu.class);
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methods;
    private MyClass mc;

    /**
     * Creates methods in constructor due to interface{@link Printable}.
     * Also creates {@link Locale} and {@link ResourceBundle}.
     */
    public Menu() {
        mc = new MyClass();
        setMenu();
        methods = new LinkedHashMap<>();
        methods.put("1", this::printPrivateFieldsWithAnnotation);
        methods.put("2", this::printFieldsOfMyAnnotation);
        methods.put("3", this::invokeMethods);
        methods.put("4", this::setValueIntoField);
        methods.put("5", this::invokeMyMethod);
        methods.put("6", this::showInfoAboutObject);
        show();
    }

    /**
     * Creating menu.
     */
    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Print fields with annotation");
        menu.put("2", "2 - Print fields of MyAnnotation");
        menu.put("3", "3 - Invoke methods");
        menu.put("4", "4 - Set value into field");
        menu.put("5", "5 - Invoke MyMethod");
        menu.put("6", "6 - Show info about object");
        menu.put("Q", "Q - Exit");
    }

    /**
     * Through reflection print those fields in the class that were annotate by this annotation.
     */
    private void printPrivateFieldsWithAnnotation() {
        MyClass myClass = new MyClass();
        Class clazz = myClass.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                field.setAccessible(true);
                try {
                    Object o = field.get(myClass);
                    System.out.println(field.getName() + " = " + o);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Print annotation value into console (e.g. @Annotation(name = "111")).
     */
    private void printFieldsOfMyAnnotation() {
        try {
            Class clazz = MyClass.class;
            Field age = clazz.getDeclaredField("age");
            MyAnnotation myAnnotation = age.getAnnotation(MyAnnotation.class);
            System.out.println("@MyAnnotation({name = " +
                    myAnnotation.name() + ", count = " + myAnnotation.count() + "})");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    /**
     * Invoke method (three method with different parameters and return types).
     */
    private void invokeMethods() {
        try {

            MyClass myClass = new MyClass();
            Class clazz = myClass.getClass();

            Method getInfo1 = clazz.getDeclaredMethod("getInfo");
            getInfo1.setAccessible(true);
            System.out.println("Method \"getInfo1\":");
            getInfo1.invoke(myClass);

            Method getInfo2 = clazz.getDeclaredMethod("getInfo", new Class[]{String.class, int.class});
            getInfo2.setAccessible(true);
            System.out.println("\nMethod \"getInfo2\":");
            getInfo2.invoke(myClass, "Oleg", 17);

            Method getCity = clazz.getDeclaredMethod("getCity");
            getCity.setAccessible(true);
            System.out.println("\nMethod \"getCity\":");
            String str = (String) getCity.invoke(myClass);
            System.out.println("Return - " + str);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set value into field not knowing its type.
     */
    private void setValueIntoField() {
        try {
            MyClass myClass = new MyClass();
            Class clazz = myClass.getClass();

            Field city = clazz.getDeclaredField("city");
            city.setAccessible(true);
            Class<?> type = city.getType();
            Object o = type.newInstance();
            city.set(myClass, o);
            myClass.publicGetInfo();

        } catch (NoSuchFieldException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Invoke {@link MyClass#myMethod(String, int[])} and myMethod(String … args).
     */
    private void invokeMyMethod() {
        try {
            MyClass myClass = new MyClass();
            Class clazz = myClass.getClass();

            System.out.println("myMethod(String a, int ... args):");
            Method myMethod = clazz.getDeclaredMethod("myMethod", String.class, int[].class);
            myMethod.setAccessible(true);
            int[] a = {1, 2, 3, 4, 5};
            myMethod.invoke(myClass, "Stryi", a);

            System.out.println("\nmyMethod(String... args):");
            Method myMethod2 = clazz.getDeclaredMethod("myMethod", String[].class);
            myMethod2.setAccessible(true);
            String[] strings = {"Oleg", "Stryi"};
            myMethod2.invoke(myClass, (Object) strings);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            log.info(e);
        }
    }

    /**
     * Shows info about class
     */
    private void showInfoAboutObject() {
        ClassInfo classInfo = new ClassInfo();
        MyClass myClass = new MyClass();

        classInfo.showObjectInfo(myClass);
    }

    /**
     * Show all information about {@link MyClass}.
     */
    private void getMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            System.out.println(menu.get(key));
        }
    }

    /**
     * Main method to work with user in console.
     */
    private void show() {
        String keyMenu;
        do {
            getMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methods.get(keyMenu).print();
            } catch (Exception e) {
                log.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }

}
