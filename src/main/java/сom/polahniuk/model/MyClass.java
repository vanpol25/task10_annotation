package сom.polahniuk.model;

import java.util.Date;

/**
 * Simple class with private fields and getters setters.
 * Also has another methods to work with reflection.
 */
public class MyClass {

    @MyAnnotation
    private int age = 21;
    private String name = "Ivan";
    @MyAnnotation(name = "111")
    private String city = "Lviv";
    private Date date = new Date();

    /**
     * Shows all info about fields.
     */
    private void getInfo() {
        System.out.println("age = " + age +
                "\nname = " + name +
                "\ncity = " + city +
                "\ndate = " + date);
    }

    /**
     * Shows all info about fields with param.
     *
     * @param name - change value of {@link MyClass#name}.
     * @param age  - change value of {@link MyClass#age}.
     */
    private void getInfo(String name, int age) {
        this.name = name;
        this.age = age;
        getInfo();
    }

    /**
     * @param a    - change value of {@link MyClass#city}.
     * @param args - change value of {@link MyClass#age}.
     */
    private void myMethod(String a, int... args) {
        this.city = a;
        this.age = 0;
        for (int n : args) {
            this.age += n;
        }
        getInfo();
    }

    /**
     * @param args - change value of {@link MyClass#name} and {@link MyClass#city}
     */
    private void myMethod(String... args) {
        this.name = args[0];
        this.city = args[1];
        getInfo();
    }

    /**
     * Public method to show fields info.
     */
    public void publicGetInfo() {
        getInfo();
    }

    private String getCity() {
        return city;
    }

    private void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
