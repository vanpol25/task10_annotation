package сom.polahniuk.model;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Class shows all information about any class comes to {@link ClassInfo#showObjectInfo(Object)}.
 */
public class ClassInfo {

    /**
     * @param o - any class we want to show info.
     */
    public void showObjectInfo(Object o) {
        Class clazz = o.getClass();
        String name = clazz.getName();
        String simpleName = clazz.getSimpleName();
        Field[] declaredFields = clazz.getDeclaredFields();
        Method[] declaredMethods = clazz.getDeclaredMethods();
        showName(name, simpleName);
        showFields(declaredFields);
        showMethods(declaredMethods);
    }

    /**
     * Shows to console name and package of class.
     *
     * @param name       - package of class.
     * @param simpleName - name of class.
     */
    private void showName(String name, String simpleName) {
        System.out.println("Class - " + simpleName);
        System.out.println("Package - " + name);
    }

    /**
     * Show all declared fields of class and show it to console.
     *
     * @param fields - Fields of class.
     */
    private void showFields(Field[] fields) {
        System.out.println("Fields:");
        for (Field field : fields) {
            int modifiers = field.getModifiers();
            char a = modifiers == 1 ? '+' : '-';
            System.out.println(" " + a + field.getName());
        }
    }

    /**
     * Show all declared methods of class and show it to console.
     *
     * @param methods - Methods of class.
     */
    private void showMethods(Method[] methods) {
        System.out.println("Methods:");
        for (Method method : methods) {
            int modifiers = method.getModifiers();
            char a = modifiers == 1 ? '+' : '-';
            System.out.println(" " + a + method.getName());
        }
    }

}
